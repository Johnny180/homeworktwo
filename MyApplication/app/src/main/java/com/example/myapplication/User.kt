package com.example.myapplication

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class User: RealmObject() {

    @PrimaryKey
    var id : Int = 0

    var firstName : String = ""

    var lastName : String = ""

}