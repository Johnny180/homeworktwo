package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import io.realm.Realm
import io.realm.RealmConfiguration
import kotlinx.android.synthetic.main.activity_main.*

class ActivityMain : AppCompatActivity() {
    companion object{
        lateinit var fragmentManger: FragmentManager
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Realm.init(this)
        val config = RealmConfiguration.Builder().name("myrealm.realm").build()
        Realm.setDefaultConfiguration(config)

        fragmentManger = supportFragmentManager

        if(frame != null) {
            if(savedInstanceState != null) {
                return
            }

            val transaction= fragmentManger.beginTransaction()
            val fragment = Fragment()
            transaction.add(R.id.frame,fragment,null)
            transaction.commit()
        }
    }
}