package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.user_card.view.*

class UserRecyclerAdapter  : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<User> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.user_card, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {

            is UserViewHolder -> {
                holder.bind(items[position])
            }

        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(userList: MutableList<User>) {
        items = userList
    }

    class UserViewHolder
    constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){

        private val userFirstName: TextView = itemView.user_first_name
        private val userLastName: TextView = itemView.user_last_name

        fun bind(userPost: User){
            userFirstName.text = userPost.firstName
            userLastName.text = userPost.lastName
        }

    }
}