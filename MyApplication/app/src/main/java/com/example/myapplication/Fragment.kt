package com.example.myapplication

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment.*


class Fragment : Fragment() {
    private lateinit var userAdapter: UserRecyclerAdapter
    private lateinit var realm: Realm

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        realm = Realm.getDefaultInstance()

        val imm: InputMethodManager =
            activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

        val btnAddUser = view.findViewById <Button> (R.id.add_user)
        val btnRemoveUser = view.findViewById <Button> (R.id.remove_user)
        val txtFirstName = view.findViewById <EditText> (R.id.first_name)
        val txtLastName = view.findViewById <EditText> (R.id.last_name)

        initRecyclerView()

        btnAddUser.setOnClickListener {

            val currentIdNumber: Number? = realm.where(User::class.java).max("id")
            val nextId: Int

            nextId = if(currentIdNumber == null) {
                1
            } else {
                currentIdNumber.toInt() + 1
            }

            val user = User()
            user.id = nextId
            user.firstName = txtFirstName.text.toString().replace("\\s".toRegex(), "")
            user.lastName = txtLastName.text.toString().replace("\\s".toRegex(), "")

            addUser(user)

            imm.hideSoftInputFromWindow(view.windowToken, 0)

            txtFirstName.setText("")
            txtLastName.setText("")
        }

        btnRemoveUser.setOnClickListener {

            val user = User()
            user.firstName = txtFirstName.text.toString().replace("\\s".toRegex(), "")
            user.lastName = txtLastName.text.toString().replace("\\s".toRegex(), "")

            removeUser(user)

            imm.hideSoftInputFromWindow(view.windowToken, 0)

            txtFirstName.setText("")
            txtLastName.setText("")
        }

    }

    private fun addUser(user: User){
        try{

            val data = realm.where(User::class.java)
                .equalTo("firstName", user.firstName).and()
                .equalTo("lastName", user.lastName).findFirst()

            if (data != null) {

                Toast.makeText(this.context, "User already added.", Toast.LENGTH_SHORT).show()

            } else {

                realm.beginTransaction()

                realm.copyToRealmOrUpdate(user)

                realm.commitTransaction()

                Toast.makeText(this.context, "User added successfully.", Toast.LENGTH_SHORT).show()
            }

        } catch(e: Exception) {
            Toast.makeText(this.context, "Could not add user", Toast.LENGTH_SHORT).show()
        }

        val results: RealmResults<User> = realm.where <User> (User::class.java).findAll()

        userAdapter.submitList(results)
        userAdapter.notifyDataSetChanged()
    }

    private fun removeUser(user: User) {
        try{

            realm.beginTransaction()

            val toDelete = realm.where(User::class.java)
            .equalTo("firstName", user.firstName).and()
            .equalTo("lastName", user.lastName).findFirst()

            toDelete!!.deleteFromRealm()
            realm.commitTransaction()

            Toast.makeText(this.context, "User removed successfully.", Toast.LENGTH_SHORT).show()

            val results: RealmResults<User> = realm.where <User> (User::class.java).findAll()

            userAdapter.submitList(results)
            userAdapter.notifyDataSetChanged()

        } catch(e: Exception) {
        Toast.makeText(this.context, "User not found.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initRecyclerView(){
        recycler.apply {
            layoutManager = LinearLayoutManager(activity)
            val topSpacingDecorator = TopSpacingItemDecoration(30)
            addItemDecoration(topSpacingDecorator)
            userAdapter = UserRecyclerAdapter()
            adapter = userAdapter
        }

        val results: RealmResults<User> = realm.where <User> (User::class.java).findAll()
        userAdapter.submitList(results)
    }
}
